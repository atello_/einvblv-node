/**
*  @file Autenticate.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a middleware util class implementation for validate user autenticate and  scopes to access request.
*/

import HttpResponse from "../util/HttpResponse";
import axios from 'axios';
import * as config from 'config'

export default (scopes?: string[]) => (req, res, next) => {
    //return next();//skip check auth IAM
    const IAM: any = config.get('IAM');
    if (IAM.ACTIVE) {
        console.log('scopes', scopes);
        if (!req.headers.authorization) {
            return res.send(HttpResponse.create(101, 'Header authorization is required.', {}));
        } else {
            try {
                const axiosInstance = axios.create({
                    baseURL: IAM.URL_BASE,
                    timeout: 5000,
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded', 'Authorization': req.headers.authorization }
                });
                axiosInstance.post(IAM.URL_AUTHENTICATE).then((response) => {
                    if (response.data && response.data.code == 0) {
                        if (scopes) {
                            const userScopes = response.data.data.scope.split(' ');
                            scopes.forEach((scope) => {
                                if (userScopes.indexOf(scope) == -1) {
                                    return res.send(HttpResponse.create(105, 'Scope access denied', scope))
                                }
                            });
                        }
                        req.user = response.data.data;
                        next();
                    } else {
                        return res.send(HttpResponse.create(104, 'IAM Authentication error', response.data.msg))
                    }
                }).catch((err) => {
                    console.log('Error authenticating', err);
                    return res.send(HttpResponse.create(103, 'IAM Authentication error', err.message))
                });
            } catch (ex) {
                console.log('Error authenticating', ex);
                return res.send(HttpResponse.create(102, 'Authentication error', ex.message))
            }
        }
    } else {
        next();
    }
}
