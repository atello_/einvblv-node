/**
 *  @file CommonReserve.repository.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all repository operations for entity commons.
 */
import { sql } from 'slonik'
import * as Debugger from 'debug';
import DB from "./DB";
import ValueCatalog from '../model/ValueCatalog.model';
import Constants from '../commons/Constants';

const debug = Debugger('BASE:CommonReserveRepository');

export default class CommonReserveRepository{
    
    /**
     * Get types of reserves from catalog.
     */
    public static async getTypesReservesCatalog() : Promise<ValueCatalog[]> {
        try {
            return DB.pool.any(sql`select valuecatalogid, typecatalogid, companycode, valuecatalogname, 
                description from valuecatalog where typecatalogid = ${Constants.typeResourcesTypeCatalog} 
                and status = ${Constants.statusActive}`);
        } catch (error) {
            let message = 'Slonik Error => ' + error;
            throw message;
        }
    }
}
