/**
 *  @file Schedules.repository.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all repository operations for entity Schedules.
 */
import { sql } from 'slonik'
import * as Debugger from 'debug';
import DB from "./DB";
import SchedulesResponse from '../model/response/Schedules.response';

const debug = Debugger('BASE:SchedulesRepository');

export default class SchedulesRepository {
    /**
     * Get Schedules by resourceId.
     * @param resourceId Id del recurso.
     */
    public static async getSchedulesByResourceId(resourceId: number) : Promise<SchedulesResponse[]> {
        try {
            if (resourceId !== null) {
                return await DB.pool.any(sql`select
                sch.scheduleid, sch.typecataloginterval, sch.valuecataloginterval, sch.typecatalogdays, sch.valuecatalogdays,
                sch.starttime as startday, sch.endtime as endday,
                vcd.valuecatalogname as days, vci.valuecatalogname as interval
                from schedule sch
                left join valuecatalog vci on vci.typecatalogid = sch.typecataloginterval and vci.valuecatalogid = sch.valuecataloginterval
                left join valuecatalog vcd on vcd.typecatalogid = sch.typecatalogdays and vcd.valuecatalogid = sch.valuecatalogdays
                where sch.resourceid = ${resourceId}`);
            } else {
                return null;
            }
        } catch (error) {
            throw 'Slonik Error => ' + error;
        }
    }
}