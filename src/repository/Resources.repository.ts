/**
 *  @file Resources.repository.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all repository operations for entity Resources.
 */
import { sql } from 'slonik'
import * as Debugger from 'debug';
import DB from "./DB";
import ResourcesByValueCatalogType from '../model/response/ResourcesByValueCatalogType.response';

const debug = Debugger('BASE:ResourcesRepository');

export default class ResourcesRepository {
     /**
     * Get resources by valueCatalogType.
     * @param valueCatalogType Id del usuario.
     */
    public static async getResourcesByValueCatalogType(valueCatalogType: number) : Promise<ResourcesByValueCatalogType[]> {
        try {
            if (valueCatalogType >= 0) {
                return await DB.pool.any(sql`select resourceid, companyCode, name, typecatalogtype,
                valuecatalogtype, description from resources where valuecatalogtype = ${valueCatalogType}`);
            } else {
                return await DB.pool.any(sql`select resourceid, companyCode, name, typecatalogtype,
                valuecatalogtype, description from resources`);
            }
        } catch (error) {
            throw 'Slonik Error => ' + error;
        }
    }
}