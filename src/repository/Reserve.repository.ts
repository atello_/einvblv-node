/**
 *  @file Reserve.repository.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all repository operations for entity Reserve.
 */
import { sql } from 'slonik'
import * as Debugger from 'debug';
import DB from "./DB";
import Constants from '../commons/Constants';
import ReservesByUser from '../model/response/ReservesByUser.response';
import CreateReserve from '../model/request/CreateReserve.request';

var pg = require('pg');
var moment = require('moment');

const debug = Debugger('BASE:ReserveRepository');

export default class ReserveRepository {
    /**
     * Get reserves by userId.
     * @param userId Id del usuario.
     */
    public static async getReservesByUserId(userId: number): Promise<ReservesByUser[]> {
        try {
            return await DB.pool.any(sql`select reso.name, rese.starttime, rese.endtime, rese.reserveid, rese.resourceid,
                reso.typecatalogtype, reso.valuecatalogtype, vc.valuecatalogname from reserve rese
                inner join resources reso on reso.resourceid = rese.resourceid
                inner join valuecatalog vc on vc.valuecatalogid = reso.valuecatalogtype and
                vc.typecatalogid = ${Constants.typeResourcesTypeCatalog} where rese.userid = ${userId}`);
        } catch (error) {
            throw 'Slonik Error => ' + error;
        }
    }

    /**
    * Create a notification.
    */
    public static async createOrUpdateReserve(request: CreateReserve) {
        try {
            
            let busy = 0;
            let schedulesTaked = await DB.pool.query(sql`select starttime, endtime from reserve
                where resourceid = ${request.resourceId} and date(starttime) = date(${request.startTime.toLocaleString()})`);
            
            
            schedulesTaked.rows.forEach((row) => {
                let startDate = new Date(row.starttime);
                let endDate = new Date(row.endtime);

                if (moment(request.startTime) >= startDate.getTime() && moment(request.startTime) <= endDate.getTime()) {
                    busy = busy + 1;
                }

                if (moment(request.endTime) >= startDate.getTime() && moment(request.endTime) <= endDate.getTime()) {
                    busy = busy + 1;
                }
            });
            console.log('DISPONIBILIDAD => '+ busy);

            if (busy === 0) {
                if (request.reserveId === null) {
                    return await DB.pool.query(sql`INSERT INTO reserve(companycode, starttime, endtime, observation, typecatalogstate, valuecatalogstate, userid, resourceid, status, createdbyid, createddate, lastmodifiedbyid, lastmodifieddate) 
                    VALUES ${sql.tuple([
                        1, request.startTime.toLocaleString(), request.endTime.toLocaleString(), request.observation, 2, 1, request.userId, request.resourceId, '1', request.userId, new Date().toLocaleString(), null, null
                    ])}`);
                } else {
                    return await DB.pool.query(sql`UPDATE reserve
                    SET starttime=${request.startTime.toLocaleString()}, endtime=${request.endTime.toLocaleString()}, observation=${request.observation}, resourceId=${request.resourceId}, lastmodifiedbyid=${request.userId}, lastmodifieddate=${new Date().toLocaleString()}
                    WHERE reserveid=${request.reserveId} AND userid=${request.userId}`);
                }
            } else {
                throw "El horario ya no se encuentra disponible";
            }
        } catch (error) {
            console.log("Error", error);
            throw 'Slonik Error => ' + error;
        }
    }

    public static async getReservesAvailable(request: CreateReserve): Promise<ReservesByUser[]> {
        try {

        return await DB.pool.any(sql`select starttime, endtime from reserve
        where resourceid = ${request.resourceId} and date(starttime) = date(${request.startTime.toLocaleString()})`);
        
        } catch (error) {
            throw 'Slonik Error => ' + error;
        }
    }
}
