import { createPool } from 'slonik'

import * as config from 'config'
import AppConfig, { AppConfigPostgres } from '../util/AppConfig';

export default class DB {
    public static pool = createPool(process.env.POSTGRES_URI || config.get<AppConfigPostgres>(AppConfig.POSTGRES).URI)
}