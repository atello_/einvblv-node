/**
 *  @file Notification.repository.ts
 *  @author  Kenny Lopez
 *  @version 1.0
 *  @description This class define all repository operations for entity Notification.
 */
import Notification from "../model/Notification.model";

import { sql } from 'slonik'



import * as Debugger from 'debug';
import DB from "./DB";
const debug = Debugger('BASE:NotificationRepository');

export default class NotificationRepository {

  /**
   * Create all notifications.
   */
  public static async createAll(notifications: Notification[]) {
    let values: any[][];
    notifications.forEach(notification => {
      values.push([notification.title, notification.description, notification.imageUrl, notification.creationDate, notification.status]);
    });

    return DB.pool.query(sql`INSERT INTO notification(title, description, imageUrl, creationDate, status) 
        VALUES ${sql.tupleList(values)}`);
  }

  /**
   * Create a notification.
   */
  public static async create(notification: Notification) {
    return DB.pool.query(sql`INSERT INTO notification(title, description, imageUrl, creationDate, status) 
        VALUES ${sql.tuple([
      notification.title, notification.description, notification.imageUrl ? notification.imageUrl : null, notification.creationDate, notification.status
    ])}
    `);
  }

  /**
   * Get all Notifications.
   */
  public static async getAll() : Promise<Notification[]> {
    return DB.pool.any(sql`select title, description,imageUrl from notification`)
  }

}
