export default class Constants {

    static ERR_INTERNAL = 500;
    static ERR_NOT_SAVE = 100;
    static ERR_ENTITY_NOT_EXIST = 101;
    static ERR_ALREADY_EXIST = 103;
    static ERR_VALIDATION = 105;
    static SUCCESS = 200;

    static typeResourcesTypeCatalog = 1;
    static statusActive = '1';

}