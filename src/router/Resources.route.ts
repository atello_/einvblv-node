/**
*  @file Resource.router.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description This file define all routers for entity Resource.
*/
import * as express from 'express';
const router = express.Router();
import authenticate from "../middlewares/Authenticate";
import ResourceController from '../controller/Resource.controller';

/**
* @api {get} /krgServices/resources/resourcesByValueCatalogType Get Resources by valueCatalogType.
* @apiVersion 1.0.0
* @apiName Resources by valueCatalogType
* @apiGroup Resources
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of Notifications
*
* @apiUse BearerAuthenticationError
*/
router.get('/resourcesByValueCatalogType',authenticate(), ResourceController.getResourcesByValueCatalogType);

export default router;