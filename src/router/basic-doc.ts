/**
*  @file basic-doc.js
*  @author  Kenny Lopez
*  @version 1.0
*  @description Basic API documentation to be included in the documentation generated for the component.
*/

/**
 * @apiDefine basic_authorization Basic Authorization needed.
 * 
 * The Basic authorization is used to authenticate the client application not the
 * user that use the application. To allow the validation of the model using a
 * basic authorization, the client must perform the request with the header:
 *
 * <code>Authorization Basic {authorization code}</code>
 *
 * The <code>authorization code</code> is generated from the clientId and clientSecret
 * as base64 string:

 * <code>base64("clientId:clientSecret")</code>
 *
 * The values of clientId and clientSecret are provided by the developer,
 * depending on the grant type used to handle the client request. Currently
 * the used grant type is <code>password</code>.
 *
 * @apiVersion 1.0.0
 */

 /**
 * @apiDefine bearer_authorization Bearer Authorization needed.
 * 
 * The Bearer authorization is used to authenticate the user that used the client application.
 * In some case it will help to identify the client application to, because the access token
 * of one user is related to the client for which was generated. To allow the validation of
 * the model using a bearer authorization, the client must perform the request with the header:
 *
 * <code>Authorization Bearer {access token}</code>
 *
 * The access token of the user is delivered to the client application after the user
 * signin successfully in the system. That token can expire in time but the
 * refresh token can be used to renovate the token. This authorization method
 * is totally different to the basic authorization and you never can use both
 * methods together.
 * 
 * @apiVersion 1.0.0
 */

/**
 * @apiDefine BearerAuthenticationError
 * @apiVersion 1.0.0
 *
 * @apiError (Error 400 - Bearer authentication errors) {Number} code  Error code
 * to allow developers to differentiate errors.
 *
 * <code>ERR_TOKEN_REQUIRED</code> Empty access token.
 *
 * <code>ERR_OAUTH_INVALID_CREDENTIALS</code> Access token don't exist on the system. The
 * application must request to the user credential to request a new access token from the server.
 *
 * <code>ERR_TOKEN_EXPIRED</code> Access token expired. Client application must refresh the access token.
 */


 /**
 * @apiDefine EntityNotExistError
 * @apiVersion 1.0.0
 *
 * @apiError (Error 101 - Entity not exist) {Number} code  Error code
 * to allow developers identify that entity not exist. 
 */

 /**
 * @apiDefine ValidationError
 * @apiVersion 1.0.0
 *
 * @apiError (Error 105 - Error validations ) {Number} code  Error code
 * to allow developers identify error validations. 
 */

 /**
 * @apiDefine AlreadyExistError
 * @apiVersion 1.0.0
 *
 * @apiError (Error 103 - Already exist ) {Number} code  Error code
 * to allow developers identify when already exist. 
 */

/**
 * @apiDefine OperationNotSuccessfulError
 * @apiVersion 1.0.0
 *
 * @apiError (Error 100 - Operation not successful ) {Number} code  Error code
 * to allow developers identify when already exist. 
 */