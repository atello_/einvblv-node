/**
*  @file CommonReserve.router.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description This file define all routers for entity Notification.
*/
import * as express from 'express';
const router = express.Router();
import authenticate from "../middlewares/Authenticate";
import CommonReserveController from '../controller/CommonReserve.controller';

/**
* @api {get} /krgServices/commonReserve/typesReservesCatalog Get types of reserves from catalog.
* @apiVersion 1.0.0
* @apiName TypesReservesCatalog
* @apiGroup Catalogs
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of Notifications
*
* @apiUse BearerAuthenticationError
*/
router.get('/typesReservesCatalog',authenticate(), CommonReserveController.getTypesReservesCatalog);

export default router;