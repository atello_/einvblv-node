/**
*  @file Reserve.router.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description This file define all routers for entity Reserve.
*/
import * as express from 'express';
const router = express.Router();
import authenticate from "../middlewares/Authenticate";
import ReserveController from '../controller/Reserve.controller';

/**
* @api {get} /krgServices/reserves/reservesByUserId Get reserves by user id.
* @apiVersion 1.0.0
* @apiName Reserves by userId
* @apiGroup Reserves
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of Notifications
*
* @apiUse BearerAuthenticationError
*/
router.get('/reservesByUserId',authenticate(), ReserveController.getReservesByUserId);

/**
* @api {post} /krgServices/reserves/crateReserve Create reserves.
* @apiVersion 1.0.0
* @apiName Create Reserves
* @apiGroup Reserves
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of Notifications
*
* @apiUse BearerAuthenticationError
*/
router.post('/createOrUpdateReserve',authenticate(), ReserveController.createReserve);

/**
* @api {post} /krgServices/reserves/reservesAvailable get reserves available.
* @apiVersion 1.0.0
* @apiName Get reserves available
* @apiGroup Reserves
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of Notifications
*
* @apiUse BearerAuthenticationError
*/
router.post('/reservesAvailable',authenticate(), ReserveController.getReservesAvailable);

export default router;