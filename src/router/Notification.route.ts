/**
*  @file Notification.router.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This file define all routers for entity Notification.
*/
import * as express from 'express';
const router = express.Router();
import authenticate from "../middlewares/Authenticate";
import NotificationController from "../controller/Notification.controller";


/**
* @api {post} /base/v1/notification/create Create a notification
* @apiVersion 1.0.0
* @apiName Create
* @apiGroup Notification
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of notification created
*
* @apiUse BearerAuthenticationError
*/
router.post('/create',authenticate(), NotificationController.create);

/**
* @api {get} /base/v1/notification/all Get all Notifications
* @apiVersion 1.0.0
* @apiName All
* @apiGroup Notification
* @apiPermission bearer_authorization
*
* @apiSuccess (Success 200) {String} error The error code
* @apiSuccess (Success 200) {String} message The message detail
* @apiSuccess (Success 200) {Array} data Array of Notifications
*
* @apiUse BearerAuthenticationError
*/
router.get('/all',authenticate(), NotificationController.getAll);

export default router;