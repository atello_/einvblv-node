import * as express from 'express';
import * as logger from 'morgan';
import * as methodOverride from 'method-override'
import * as cors from 'cors';
import * as bodyParser from 'body-parser';
import * as Debugger from 'debug';

import HttpResponse from './util/HttpResponse';

const debug = Debugger('BASE:App');

//import app from "./router/route";
const app = express();

// Express configuration
app.set("port", process.env.PORT || 3000);
app.set("env", process.env.ENV || 'debug');
app.set("timeout", process.env.TIMEOUT || 12000);
// set the view engine to ejs
app.set('view engine', 'ejs');
//app.set("DEBUG", process.env.DEBUG || 'BASE:*');

// headers and cors
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method');
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
  res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');
  next();
});

app.use(logger(app.get('ENV')));

app.use(express.json({ limit: '50mb' }));
app.use(express.urlencoded({ extended: true, limit: '50mb' }));
/*
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
*/

app.use(methodOverride());
app.use(cors());


// static resources
app.use('/krgServices/api-doc', express.static('api-doc'));
app.use(express.static('api-doc'));

//ROUTER
import notificationRoute from "./router/Notification.route";
app.use('/krgServices/notification', notificationRoute);

// Router for common resources.
import commonReserveRoute from './router/CommonReserve.route';
app.use('/krgservices/commonReserve', commonReserveRoute);

// Router for Reserves
import reserves from './router/Reserve.route';
app.use('/krgservices/reserves', reserves);

// Router for Resources
import resources from './router/Resources.route';
app.use('/krgservices/resources', resources);



//Error handling middleware, we delegate the handling to the centralied error handler
app.use((err, req, res, next) => {
  debug('error', err);
  return res.send(HttpResponse.create(500, 'Internal server error', err));
});

app.use((req, res) => {
  debug('error', req.url + 'not exist');
  res.status(404).send(HttpResponse.create(404, 'service not found', null));
});

export default app;
