/**
*  @file MailService.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a util class for comunicate with Mail service.
*/
import * as config from 'config'
import * as Debugger from 'debug';

const debug = Debugger('BASE:mail');

export default class MailService {
    /**
     * send mail.
     *
     * @param to  the to mail
     * @param subject  the subject
     * @param text  the text body
     * @param attachmentPath  the attachment path can be optional
     */
    public static send(to: string, subject: string, text: string, attachmentPath?: string): Promise<any> {
        const MAIL: any = config.get('MAIL');
        const api_key = MAIL.API_KEY;
        const domain = MAIL.DOMAIN;
        const mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
        return new Promise((resolve, reject) => {
            const data = {
                from: MAIL.FROM,
                to: to,
                subject: subject,
                text: text,
                attachment: attachmentPath
            };



            mailgun.messages().send(data, (error, body) => {
                debug('mailgun', error, body);
                if (error) {
                    return reject(error);
                }
                return resolve(body);
            });
        });
    }

    /**
     * send mail html.
     *
     * @param to  the to mail
     * @param subject  the subject
     * @param html  the html body
     * @param attachmentPath  the attachment path can be optional
     */
    public static sendHtml(to: string, subject: string, html: string, attachmentPath?: string): Promise<any> {
        const MAIL: any = config.get('MAIL');
        const api_key = MAIL.API_KEY;
        const domain = MAIL.DOMAIN;
        const mailgun = require('mailgun-js')({ apiKey: api_key, domain: domain });
        return new Promise((resolve, reject) => {
            const data = {
                from: MAIL.FROM,
                to: to,
                subject: subject,
                html: html,
                attachment: attachmentPath
            };
            mailgun.messages().send(data, (error, body) => {
                debug('mailgun', error, body);
                if (error) {
                    return reject(error);
                }
                return resolve(body);
            });
        });
    }
}