/**
*  @file JoiValidatorUtil.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a util class for integrate joi validator library.
*/
import * as Joi from '@hapi/joi';
import { Request } from 'express';

/**
 * Joi validator util class
 */
export default class JoiValidatorUtil {
   
     /**
     * @description Validate params for location point
     * @param req The request
     * @return Array string msg validator
     */
    public static validateLocation(req: Request): String[] {
        let obj = {
            latitude: req.query.latitude,
            longitude: req.query.longitude,
        }
        const validatorLocation = Joi.object().keys({
            latitude: Joi.number().min(-90).max(90),
            longitude: Joi.number().min(-180).max(180),
        }).with('longitude', ['latitude']).with('latitude', ['longitude']);
        const result = Joi.validate(obj, validatorLocation, { abortEarly: false });
        return JoiValidatorUtil.errors(result);
    }

    /**
     * @description Validate params for paginate data
     * @param req The request
     * @return Array string msg validator
     */
    public static validatePaginate(req: Request): String[] {
        let obj = {
            size: req.query.size,
            page: req.query.page,
        }
        const validatorPaginate = Joi.object().keys({
            page: Joi.number().required(),
            size: Joi.number().required(),
        });
        const result = Joi.validate(obj, validatorPaginate, { abortEarly: false });
        return JoiValidatorUtil.errors(result);
    }

    /**
     * @description Validate _id mongoose 
     * @param object Object to validate _id mongoose format
     * @return Array string msg validator
     */
    public static validateId(object: any): String[] {
        let obj = {
            _id: object._id,
        }
        const validatorId = Joi.object().keys({
            _id: Joi.string().trim().alphanum().required().length(24)
        });
        const result = Joi.validate(obj, validatorId, { abortEarly: false });        
        return JoiValidatorUtil.errors(result);
    }

     /**
     * @description Validate _id mongoose 
     * @param req The request
     * @return Array string msg validator
     */
    public static validateIdQuery(req: Request): String[] {
        let obj = {
            _id: req.query._id,
        }
        const validatorId = Joi.object().keys({
            _id: Joi.string().trim().alphanum().required().length(24)
        });

        const result = Joi.validate(obj, validatorId, { abortEarly: false });
        return JoiValidatorUtil.errors(result);
    }

    /**
     * @description Validate a object with a Joi validator instance.
     * @param object  the object to validate
     * @param validator  the validator joi instance
     */
    public static validate(object: any, validator: any): String[] {
        if(!object){
          return  ['object to validate must not be null'];
        }
        const result = Joi.validate(object, validator, { abortEarly: false });       
        return JoiValidatorUtil.errors(result);
    }

    /**
     * @description Return array errors from joi result or null.
     * @param result Joi result validator
     * @return Array string msg error 
     */
    private static errors(result): String[] {
        let error = result.error;
        if (error) {
            let errors = [];
            error.details.forEach(err => {
                errors.push(err.message);
            });
            return errors;
        }
        return null;
    }

} 
