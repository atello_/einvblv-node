/**
*  @file PaginateResponse.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a util class for standard paginate reponse body.
*/
export default class PaginateResponse {
    content: any[];
    total: number;
    constructor(content: any[], total: number) {
        this.content = content;
        this.total = total;
    }
    /**
     * 
     * @param content 
     * @param total 
     */
    static create(content: any[], total: number) {
       return new PaginateResponse(content,total);
    }
}