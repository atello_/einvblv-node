/**
*  @file AppConfig.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a util class for general convertions
*/

export default class AppConfig {
    static MONGO = "MONGO";
    static POSTGRES = "POSTGRES";
}

export class AppConfigMongo {
    URI: string;
}

export class AppConfigPostgres {
    URI: string;
}