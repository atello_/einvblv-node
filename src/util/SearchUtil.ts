/**
*  @file SearchUtil.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a util class for generate regex for search in mongo.
*/
import * as config from 'config'

export default class SearchUtil {
    private static special = new Map([["a", "(á|a)"], ["e", "(é|e)"], ["i", "(í|i|ï)"], ["o", "(ó|o)"], ["u", "(ú|u|ü)"], ["á", "(á|a)"], ["é", "(é|e)"], ["í", "(í|i|ï)"], ["ó", "(ó|o)"], ["ú", "(ú|u|ü)"], ["í", "(ï|i|ï)"], ["ü", "(ú|u|ü)"], ["n", "(ñ|n)"]]);
    private static COUNT_CHARACTER_SEARCH: any = process.env.COUNT_CHARACTER_SEARCH || config.get('COUNT_CHARACTER_SEARCH');
    /**
     * Replace special character by or posible options
     * @param text
     */
    static replaceSpecial(text) {
        let newText = text.toLowerCase();
        SearchUtil.special.forEach((value: string, key: string) => {
            let regex = new RegExp(key, 'g');
            newText = newText.replace(regex, value);
        });
        return newText;
    }

    /**
     * Generate regex token.
     *
     * @param text The text to search
     */
    static regexToken(text) {
        const words = text.split(" ")
        const wordsFiltered = words.filter((value) => {
            return value.length >= SearchUtil.COUNT_CHARACTER_SEARCH;
        });
        wordsFiltered.forEach((element, index) => {
            if (element.length <= SearchUtil.COUNT_CHARACTER_SEARCH) {
                wordsFiltered[index] = `(\S*(${element})\S+|\S+(${element})\S*)`;
            }
        });
        return SearchUtil.replaceSpecial(wordsFiltered.join("|"));
    }
}