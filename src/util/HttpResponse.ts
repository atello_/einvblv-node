/**
*  @file HttpResponse.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description This is a util class for standard http reponse body.
*/
export default class HttpResponse<T> {
    code: number;
    msg: string;
    data: T;
    constructor(code: number, msg: string, data: T) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    static create(code: number, msg: string, data: any) {
        return new HttpResponse(code, msg, data);
    }
}