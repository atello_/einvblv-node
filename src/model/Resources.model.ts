/**
*  @file Resources.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for resources model. 
*/

import * as Joi from '@hapi/joi';

export default class Resources {
    resourceid: number;
    companyCode: number;
    name: string;
    typecatalogtype: number;
    valuecatalogtype: number;
    description: string;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const ResourcesValidator = Joi.object().keys({
    name: Joi.string().trim(50),
    description: Joi.string().trim(256),
    status: Joi.boolean(),
    createdDate: Joi.Date(),
    lastModifiedDate: Joi.Date()
});

export const ResourcesChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});