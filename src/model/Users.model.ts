/**
*  @file Users.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for users model. 
*/

import * as Joi from '@hapi/joi';

export default class Users {
    userid: number;
    companyCode: number;
    userName: string;
    email: string;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const UsersValidator = Joi.object().keys({
    userName: Joi.string().trim(25),
    email: Joi.string().trim(50),
    status: Joi.boolean(),
    createdDate: Joi.Date(),
    lastModifiedDate: Joi.Date()
});

export const UsersChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});

