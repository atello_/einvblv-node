/**
*  @file ValueCatalog.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for valueCatalog model. 
*/

import * as Joi from '@hapi/joi';

export default class ValueCatalog {
    valuecatalogid: number;
    companycode: number;
    typecatalogid: number;
    valuecatalogname: string;
    description: string;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const ValueCatalogValidator = Joi.object().keys({
    valuecatalogname: Joi.string().trim(256),
    description: Joi.string().trim(256),
    status: Joi.boolean(),
    createdDate: Joi.Date(),
    lastModifiedDate: Joi.Date()
});

export const ValueCatalogChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});