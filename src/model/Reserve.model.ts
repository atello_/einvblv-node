/**
*  @file Reserve.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for reserve model. 
*/

import * as Joi from '@hapi/joi';

export default class Reserve {
    reserveid: number;
    companycode: number;
    starttime: Date;
    endtime: Date;
    observation: string;
    typecatalogstate: number;
    valuecatalogstate: number;
    userid: number;
    resourcesid: number;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const ReserveValidator = Joi.object().keys({
    // starttime: Joi.Date(),
    // endtime: Joi.Date(),
    // observation: Joi.string().trim(256),
    // status: Joi.boolean(),
    // createdDate: Joi.Date(),
    // lastModifiedDate: Joi.Date()
});

export const ReserveChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});