/**
*  @file Notification.model.ts
*  @author  Kenny Lopez
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for notification model. 
*/

import * as Joi from '@hapi/joi';

export default class Notification  {
    title: string;
    description: string;
    imageUrl: string;
    creationDate: number;
    modificationDate: number;
    status: boolean;
}

export const NotificationValidator = Joi.object().keys({
    _id: Joi.string().trim().length(24),
    title: Joi.string().trim().required(),
    description: Joi.string().trim().required(),
    imageUrl: Joi.string().trim().uri({ scheme: ['http', 'https'] }),
    status:Joi.boolean(),
});

export const NotificationChangeStatusValidator = Joi.object().keys({
    _id: Joi.string().trim().length(24),
    status:Joi.boolean(),
});

