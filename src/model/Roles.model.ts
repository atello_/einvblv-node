/**
*  @file Roles.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for roles model. 
*/

import * as Joi from '@hapi/joi';

export default class Roles {
    roleId: number;
    companycode: number;
    roleName: number;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const RolesValidator = Joi.object().keys({
    roleName: Joi.string().trim(50),
    status: Joi.boolean(),
    createdDate: Joi.Date(),
    lastModifiedDate: Joi.Date()
});

export const RolesChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});