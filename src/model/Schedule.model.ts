/**
*  @file Schedule.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for schedule model. 
*/

import * as Joi from '@hapi/joi';

export default class Schedule {
    scheduleid: number;
    resourceid:number
    companycode: number;
    starttime: Date;
    endtime: Date;
    typecataloginterval: number;
    valuecataloginterval: number;
    typecatalogdays: number;
    valuecatalogdays: number;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const ScheduleValidator = Joi.object().keys({
    starttime: Joi.Date(),
    endtime: Joi.Date(),
    status: Joi.boolean(),
    createdDate: Joi.Date(),
    lastModifiedDate: Joi.Date()
});

export const ScheduleChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});