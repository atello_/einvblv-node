/**
*  @file ReservesForUser.response.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for ReservesForUser model. 
*/
export default class ReservesByUser {
    name: string
    startTime: Date;
    endTime: Date;
    reserveId: number;
    resourceId: number;
    typeCatalogType: number;
    valueCatalogType: number;
    valueCatalogName: string;
}