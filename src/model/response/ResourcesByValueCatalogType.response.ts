import SchedulesResponse from "../response/Schedules.response"

/**
*  @file ResourcesByValueCatalogType.response.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define response for ResourcesByValueCatalogType model. 
*/
export default class ResourcesByValueCatalogType {
    resourceid: number;
    companyCode: number;
    name: string;
    typecatalogtype: number;
    valuecatalogtype: number;
    description: string;
    schedules: SchedulesResponse[]; 
}