/**
*  @file Schedules.response.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define a VO for Schedules model. 
*/
export default class SchedulesResponse {
    scheduleid: number;
    typecataloginterval: number;
    valuecataloginterval: number;
    typecatalogdays: number;
    valuecatalogdays: number;
    startday: Date;
    endday: Date;
    days: string;
    interval: string;
}