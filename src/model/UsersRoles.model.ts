/**
*  @file UsersRoles.model.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define Schema mongoose, schema Joi validation and interface for usersRoles model. 
*/

import * as Joi from '@hapi/joi';

export default class UsersRoles {
    userid: number;
    roleId: number;
    status: boolean;
    createdById: string;
    createdDate: Date;
    lastModifiedById: string;
    lastModifiedDate: Date;
}

export const UsersRolesValidator = Joi.object().keys({
    status: Joi.boolean(),
    createdDate: Joi.Date(),
    lastModifiedDate: Joi.Date()
});

export const UsersRolesChangeStatusValidator = Joi.object().keys({
    status: Joi.boolean()
});