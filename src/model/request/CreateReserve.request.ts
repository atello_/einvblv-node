/**
*  @file CreateReserve.request.ts
*  @author  Cristian Freire
*  @version 1.0
*  @description In this file define the request for create a reserve. 
*/
export default class CreateReserve {
    reserveId: number;
    startTime: Date;
    endTime: Date;
    observation: string;
    userId: number;
    resourceId: number;
}