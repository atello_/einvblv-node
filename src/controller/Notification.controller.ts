/**
 *  @file Notification.controller.ts
 *  @author  Kenny Lopez
 *  @version 1.0
 *  @description This class define all controller operations for entity Notification.
 */
import { Request, Response, NextFunction } from "express";
import Notification, {
  NotificationValidator
} from "../model/Notification.model";
import NotificationRepository from "../repository/Notification.repository";
import HttpResponse from "../util/HttpResponse";
import JoiValidatorUtil from "../util/JoiValidatorUtil";

import * as Debugger from 'debug';

const debug = Debugger('BASE:NotificationController');

export default class NotificationController {

  /**
  * Create a notification.
  *
  * @param {*} req
  * @param {*} res
  * @param {*} next
  */
  public static async create(req: Request, res: Response, next: NextFunction) {
    debug('create', req.body);
    const errors = JoiValidatorUtil.validate(req.body, NotificationValidator);
    if (errors) {
      return res.send(HttpResponse.create(105, 'Validation error', errors));
    }
    const notification = new Notification();
    notification.creationDate = new Date().getTime();
    notification.status = true;
    notification.title = req.body.title;
    notification.description = req.body.description;
    notification.imageUrl = req.body.imageUrl;

    //call repository for create entity
    NotificationRepository.create(notification)
      .then((obj) => {
        return res.send(HttpResponse.create(0, 'success', undefined));
      })
      .catch((err) => {
        debug("error", err)
        return res.send(HttpResponse.create(101, 'internal error', err.message));
      });
  }

  /**
   * Get all Notifications.
   *
   * @param {*} req
   * @param {*} res
   * @param {*} next
   */
  public static async getAll(req: Request, res: Response, next: NextFunction) {
    debug('all', req.query, NotificationRepository.getAll());
    NotificationRepository.getAll().then(values => {
      res.send(
        HttpResponse.create(0, "Get all Notification successfully", values)
      );
    })

  }

}
