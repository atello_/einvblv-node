/**
 *  @file CommonReserve.controller.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all controller operations for entity commons.
 */
import { Request, Response, NextFunction } from "express";
import HttpResponse from "../util/HttpResponse";

import * as Debugger from 'debug';
import CommonReserveRepository from "../repository/CommonReserve.repository";
import Constants from "../commons/Constants";

const debug = Debugger('BASE:CommonReserveController');

export default class CommonReserveController {
    
    /**
     * Get types of reserves from catalog.
     */
    public static async getTypesReservesCatalog(req: Request, res: Response, next: NextFunction){
        debug('TypesReservesCatalog', req.query, CommonReserveRepository.getTypesReservesCatalog());
        CommonReserveRepository.getTypesReservesCatalog().then(values => {
            res.send(HttpResponse.create(Constants.SUCCESS, 'Get types of reserves from catalog successfully', values));
        })
        .catch((err) => {
            debug("error", err)
            return res.send(HttpResponse.create(Constants.ERR_INTERNAL, 'internal error', err.message));
        });
    }
}