/**
 *  @file Resource.controller.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all controller operations for entity Resource.
 */
import { Request, Response, NextFunction } from "express";
import HttpResponse from "../util/HttpResponse";

import * as Debugger from 'debug';
import Constants from "../commons/Constants";
import ResourcesRepository from "../repository/Resources.repository";
import SchedulesRepository from "../repository/Schedules.repository";
import { resolve } from "path";
import { rejects } from "assert";

const debug = Debugger('BASE:ResourceController');

export default class ResourceController {
    /**
     * Get resources by valueCatalogType.
     * @param valueCatalogType Id del usuario.
     */
    public static async getResourcesByValueCatalogType(req: Request, res: Response, next: NextFunction) {
        const request = {
            valueCatalogType: req.body.valueCatalogType
        };
        debug('resourcesByValueCatalogType', req.query, ResourcesRepository.getResourcesByValueCatalogType(request.valueCatalogType));
        ResourcesRepository.getResourcesByValueCatalogType(request.valueCatalogType).then(values => {
            let promises = [];
                values.forEach(resource => {
                    resource.schedules = [];
                    promises.push(new Promise((resolve, reject) => {
                        
                        SchedulesRepository.getSchedulesByResourceId(resource.resourceid).then(data => {
                            data.forEach(schedule => {
                                resource.schedules.push(schedule);
                            })
                            resolve();
                        });
                    })
                    )
                });
                Promise.all(promises).then(() => res.send(HttpResponse.create(Constants.SUCCESS, "Success", values)))
        })
        .catch((err) => {
            debug("error", err)
            return res.send(HttpResponse.create(Constants.ERR_INTERNAL, 'internal error', err.message));
        });
    }
}