/**
 *  @file Reserve.controller.ts
 *  @author  Cristian Freire
 *  @version 1.0
 *  @description This class define all controller operations for entity Reserve.
 */
import { Request, Response, NextFunction } from "express";
import HttpResponse from "../util/HttpResponse";

import * as Debugger from 'debug';
import ReserveRepository from "../repository/Reserve.repository";
import Constants from "../commons/Constants";
import JoiValidatorUtil from "../util/JoiValidatorUtil";
import Reserve, { ReserveValidator } from "../model/Reserve.model";
import CreateReserve from "../model/request/CreateReserve.request";

const debug = Debugger('BASE:CommonReserveController');

export default class ReserveController {
  /**
   * Get reserves by userId.
   * @param userId Id del usuario.
   */
  public static async getReservesByUserId(req: Request, res: Response, next: NextFunction) {
    const request = {
      userId: req.body.userId
    };
    debug('all', req.query, ReserveRepository.getReservesByUserId(request.userId));
    ReserveRepository.getReservesByUserId(request.userId).then(values => {
      return res.send(HttpResponse.create(Constants.SUCCESS, "Success", values));
    })
    .catch((err) => {
      debug("error", err)
      return res.send(HttpResponse.create(Constants.ERR_INTERNAL, 'internal error', err.message));
    });
  }

   /**
  * Create a reserve.
  *
  * @param {*} req
  * @param {*} res
  * @param {*} next
  */
 public static async createReserve(req: Request, res: Response, next: NextFunction) {
  debug('create', req.body);
  // const errors = JoiValidatorUtil.validate(req.body, ReserveValidator);
  // if (errors) {
  //   return res.send(HttpResponse.create(Constants.ERR_VALIDATION, 'Validation error', errors));
  // }
  const createRequest = new CreateReserve();
  createRequest.reserveId = req.body.reserveId;
  createRequest.startTime = req.body.startTime;
  createRequest.endTime = req.body.endTime;
  createRequest.observation = req.body.observation;
  createRequest.userId = req.body.userId;
  createRequest.resourceId = req.body.resourceId;

  console.log(createRequest);

  //call repository for create entity
  ReserveRepository.createOrUpdateReserve(createRequest)
    .then((obj) => {
      return res.send(HttpResponse.create(Constants.SUCCESS, 'success', undefined));
    })
    .catch((err) => {
      debug("error", err)
      return res.send(HttpResponse.create(Constants.ERR_INTERNAL, 'internal error', err.message));
    });
}

/**
   * Get reserves by userId.
   * @param userId Id del usuario.
   */
  public static async getReservesAvailable(req: Request, res: Response, next: NextFunction) {
    const createRequest = new CreateReserve();
    createRequest.startTime = req.body.startTime;
    createRequest.resourceId = req.body.resourceId;
    debug('all', req.query, ReserveRepository.getReservesAvailable(createRequest));
    
    ReserveRepository.getReservesAvailable(createRequest).then(values => {
      return res.send(HttpResponse.create(Constants.SUCCESS, "Success", values));
    });
  }
}