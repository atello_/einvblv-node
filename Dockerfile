FROM node:10-alpine

WORKDIR /opt/nodejs/app

ENV NODE_ENV dev

COPY . .

#Configure config registry
#RUN npm config set registry http://10.190.6.59:8181/repository/npm-all/

RUN npm install --no-package-lock

RUN npm run build

#Generate apidoc
RUN npm run apidoc

#Copy apidoc
RUN npm run apidocCopy

EXPOSE 3000

CMD ["npm","start"]