# BASE APS Services

The BASE APS service is the responsible to provide services for proxy services to APS origin services

## Predefined error constants

```js
{
  ERR_INTERNAL: 500,
  ERR_NOT_SAVE: 100,
  ERR_ENTITY_NOT_EXIST: 101,
  ERR_ALREADY_EXIST: 103,
  ERR_VALIDATION: 105,
}
```