# App Services

# Pre-reqs
To build and run this app locally you will need a few things:
- Install [Node.js](https://nodejs.org/en/)
- Install [VS Code](https://code.visualstudio.com/)


# Getting started
- Clone the repository
```
git clone --depth=1 git@***.git
```
- Install dependencies
```
cd app-services
npm install
```
- Build and run the project
```
npm start
```
- Build and run with watch-ts files
```
npm run dev
```

# TO DEPLOY ON DOCKER

- To see docker images
```
docker images
```

- To see all docker containers
```
docker ps -a
```

- To see all docker containers ruunning
```
docker ps
```

- To initialize a docker container
```
docker start [image id]
```

- Step one Create docker image
```
docker build -t ec.com.kruger.krugerapp/krugerapp-services:1.0.0 .
```

- Step two - Deploy docker with network conection variable
```
docker run --restart=unless-stopped --name krugerapp --net ic-network -e POSTGRES_URI='postgres://kruapp:dkruapp@postgres11:5432/bdddesarrollo' -d -p 3000:3000 ec.com.kruger.krugerapp/krugerapp-services:1.0.0
```